﻿using Kampus.WordSearcher.Entities;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Kampus.WordSearcher.Logic.MapPaths
{
	public class HorizontalPath : IEnumerable<Direction>
	{
		private readonly MapArea _mapWindow;
		private readonly int _startY;

		public HorizontalPath(MapArea mapWindow, int? startY = null)
		{
			_mapWindow = mapWindow ?? throw new ArgumentNullException(nameof(mapWindow));
			_startY = startY ?? _mapWindow.Y;
		}

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
		public IEnumerator<Direction> GetEnumerator()
		{
			var y = _startY;
			while (true)
			{
				while (_mapWindow.Y != y)
					yield return y < _mapWindow.Y ? Direction.Up : Direction.Down;

				yield return Direction.Right;
			}
		}
	}
}
