﻿using Kampus.WordSearcher.Entities;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Kampus.WordSearcher.Logic.MapPaths
{
	public class RowByRowPath : IEnumerable<Direction>
	{
		private const int DefaultMapWidth = 41;

		private readonly MapArea _mapWindow;
		private readonly int _startX;
		private readonly int _gapHeight;
		private int _currentX;
		private int _currentY;

		public RowByRowPath(MapArea mapWindow, int? startX = null, int? startY = null, int gapHeight = 0)
		{
			_mapWindow = mapWindow ?? throw new ArgumentNullException(nameof(mapWindow));
			_currentX = _startX = startX ?? _mapWindow.X;
			_currentY = startY ?? _mapWindow.Y;
			_gapHeight = gapHeight >= 0 ? gapHeight : throw new ArgumentOutOfRangeException(nameof(gapHeight));
		}

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
		public IEnumerator<Direction> GetEnumerator()
		{
			if (_mapWindow.MapWidth == null)
				for (var horizontalMoveCount = 0; true; ++horizontalMoveCount)
				{
					if (horizontalMoveCount >= DefaultMapWidth)
					{
						horizontalMoveCount = 0;
						_currentY += _mapWindow.Bounds.Height + _gapHeight;
					}

					while (_mapWindow.Y != _currentY)
						yield return _currentY < _mapWindow.Y ? Direction.Up : Direction.Down;

					yield return Direction.Right;
				}
			else
				while (true)
				{
					if (_currentX < _startX && _mapWindow.X >= _startX)
						_currentY += _mapWindow.Bounds.Height + _gapHeight;

					while (_mapWindow.Y != _currentY)
					{
						_currentX = _mapWindow.X;
						yield return _currentY < _mapWindow.Y ? Direction.Up : Direction.Down;
					}

					_currentX = _mapWindow.X;
					yield return Direction.Right;
				}
		}
	}
}
