﻿using Kampus.WordSearcher.Api;
using Kampus.WordSearcher.Entities;
using Kampus.WordSearcher.LetterParser;
using Kampus.WordSearcher.Logic.MapPaths;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Kampus.WordSearcher.Logic
{
	public class WordSearcher : GameClient
	{
		private Word _firstWord;

		public WordSearcher(ILetterParser letterParser, IApiController apiController)
			: base(letterParser, apiController) { }

		protected override void Work()
		{
			IEnumerable<Direction> path = new RowByRowPath(CurrentWindow);
			while (!IsGameOver)
			{
				var wordPosition = MoveToNextWord(path);
				if (wordPosition == null)
					break;

				var word = ReadWord(wordPosition.Value);
				if (IsGameOver)
					break;
				if (word == null)
					throw new Exception("Error occured while reading word");

#if DEBUG
				Console.WriteLine(word);
				Console.WriteLine();
#endif
				if (FoundWords.Add(word))
				{
					if (FoundWords.Count == 1)
					{
						_firstWord = word;
						path = new HorizontalPath(CurrentWindow, word.Location.Top);
					}
					continue;
				}

				if (word == _firstWord)
					if (CurrentWindow.MapWidth == null)
					{
						CurrentWindow.Map = new MapGeometry(Math.Abs(_firstWord.Location.Left - word.Location.Left));
						path = new RowByRowPath(
							CurrentWindow,
							null, _firstWord.Location.Bottom,
							_firstWord.Location.Height - 1);
					}
					else
						break;
			}
		}

		private Point? MoveToNextWord(IEnumerable<Direction> directions)
		{
			var newWord = FindNewWord(CurrentWindow.Bounds);
			if (newWord != null)
				return newWord;

			foreach (var direction in directions)
			{
				if (IsGameOver)
					return null;

				Move(direction);
				newWord = FindNewWord(CurrentWindow.GetBoundarySide(direction));
				if (newWord != null)
					return newWord;
			}
			return null;
		}

		private Point? FindNewWord(Rectangle area)
		{
			var intersectingRects = FoundWords
				.Select(word => word.Location)
				.Where(location => CurrentWindow.Map.AreIntersecting(location, area))
				.ToArray();

			foreach (var point in CurrentWindow.Travel(area))
				if (CurrentWindow[point] == '1'
					&& !intersectingRects.Any(bounds => CurrentWindow.Map.IsPointInside(bounds, point)))
						return point;
			return null;
		}

		private Word ReadWord(Point wordPosition)
		{
			MoveCenterTo(wordPosition);
			var boundsOfLetter = GetBoundsOfFirstLetter();

			var word = new List<Letter>();
			while (!IsGameOver)
			{
				var area = GetArea(boundsOfLetter);
				var letter = LetterParser.Parse(area.Area);
				if (letter == null)
					break;

				var letterPosition = letter.Location;
				letterPosition.Offset(boundsOfLetter.Location);
				letter.Location = letterPosition;

				word.Add(letter);
				boundsOfLetter.Offset(letter.Location.Right + 1 - boundsOfLetter.Left, 0);
			}

			if (word.Count == 0)
				return null;
			var leftBound = word.First().Location.Left;
			var rightBound = word.Last().Location.Right;
			var boundsOfWord = new Rectangle(
				leftBound,
				boundsOfLetter.Top,
				rightBound - leftBound,
				boundsOfLetter.Height);
			return new Word(new string(word.Select(x => x.Name).ToArray()),
				CurrentWindow.Map.Normalize(boundsOfWord));
		}

		private Rectangle GetBoundsOfFirstLetter()
		{
			const int leftBoundWidth = 3; // thick bound is required because of the letter 'ы'

			var bandStart = CurrentWindow.Bounds.Left - 1;
			var bandEnd = CurrentWindow.Bounds.Right + 1;

			var top = FindBoundInBand(Direction.Up, bandStart, bandEnd).Bottom;
			var bottom = FindBoundInBand(Direction.Down, bandStart, bandEnd).Top;
			var left = FindBoundInBand(Direction.Left, top, bottom, leftBoundWidth).Right;
			return new Rectangle(left, top, CurrentWindow.Bounds.Width, bottom - top);
		}

		private Rectangle FindBoundInBand(Direction direction, int bandStart, int bandEnd, int boundWidth = 1)
		{
			var bounds = direction.IsHorizontal()
				? new Rectangle(CurrentWindow.X, bandStart, CurrentWindow.Width, bandEnd - bandStart)
				: new Rectangle(bandStart, CurrentWindow.Y, bandEnd - bandStart, CurrentWindow.Height);
			var area = GetArea(bounds);

			var currentLine = area.GetBoundarySide(direction.Reverse());
			var lineOffset = direction.ToOffset();
			var boundOffset = direction.IsHorizontal()
				? new Point(lineOffset.X * CurrentWindow.Width / 2, 0)
				: new Point(0, lineOffset.Y * CurrentWindow.Height / 2);

			var wordFound = false;
			var currentBoundWidth = 0;
			while (!IsGameOver && currentBoundWidth < boundWidth)
			{
				if (!bounds.Contains(currentLine))
				{
					bounds.Offset(boundOffset);
					area = GetArea(bounds);
				}

				if (area.Travel(currentLine).Any(point => area[point] == '1'))
				{
					wordFound = true;
					currentBoundWidth = 0;
				}
				else if (wordFound)
				{
					++currentBoundWidth;
					if (currentBoundWidth == boundWidth)
						break;
				}

				currentLine.Offset(lineOffset);
			}

			if (direction == Direction.Right || direction == Direction.Down)
			{
				var reverseOffset = direction.Reverse().ToOffset();
				currentLine.Offset(reverseOffset.X * (boundWidth - 1), reverseOffset.Y * (boundWidth - 1));
			}

			return direction.IsHorizontal()
				? new Rectangle(currentLine.Location, new Size(boundWidth, currentLine.Height))
				: new Rectangle(currentLine.Location, new Size(currentLine.Width, boundWidth));
		}
	}
}
