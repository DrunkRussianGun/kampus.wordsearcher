﻿using Kampus.WordSearcher.Api;
using Kampus.WordSearcher.LetterParser;
using System;
using System.Drawing;
using Kampus.WordSearcher.Entities;

namespace Kampus.WordSearcher.Logic
{
	public class MapTraveller : GameClient
	{
		public char[][] Map { get; private set; }

		public MapTraveller(ILetterParser letterParser, IApiController apiController)
			: base(letterParser, apiController)
		{
			CurrentWindow.Map = new MapGeometry(100, 100);
		}

		protected override void Work()
		{
			var mapBounds = new Rectangle(
				new Point(),
				new Size(CurrentWindow.MapWidth.Value, CurrentWindow.MapHeight.Value)
			);
			Map = GetArea(mapBounds).Area;
		}
	}
}
