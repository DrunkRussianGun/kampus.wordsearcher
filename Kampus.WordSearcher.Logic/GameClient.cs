﻿using Kampus.WordSearcher.Api;
using Kampus.WordSearcher.Entities;
using Kampus.WordSearcher.LetterParser;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace Kampus.WordSearcher.Logic
{
	public abstract class GameClient
	{
		private static readonly TimeSpan ReservedTimeForFinishing = TimeSpan.FromSeconds(7);

		private readonly Timer _timer = new Timer();
		private readonly IApiController _apiController;

		protected readonly ILetterParser LetterParser;
		protected readonly HashSet<Word> FoundWords = new HashSet<Word>();
		protected readonly MapArea CurrentWindow = new MapArea();

		public bool IsGameOver => !_timer.Enabled;

		protected abstract void Work();

		protected GameClient(ILetterParser letterParser, IApiController apiController)
		{
			LetterParser = letterParser ?? throw new ArgumentNullException(nameof(letterParser));
			_apiController = apiController ?? throw new ArgumentNullException(nameof(apiController));

			_timer.Elapsed += (sender, args) => _timer.Stop();
		}

		public Statistics Run()
		{
			if (!IsGameOver)
				throw new InvalidOperationException("Game client is already running");

			Start();
			try
			{
				Work();
			}
			catch (Exception inner)
			{
				var result = Finish();
				throw new Exception("Game ended with error!\n" + result, inner);
			}
			return Finish();
		}

		private void Start()
		{
			var timeOnStart = DateTime.Now;
			var session = _apiController.StartGame().Result;
			CurrentWindow.Refresh(session.VisibleArea);
#if DEBUG
			Console.WriteLine("GAME STARTED");
#endif

			var timeout = timeOnStart + session.Duration
				- DateTime.Now - ReservedTimeForFinishing;
			_timer.Interval = timeout.TotalMilliseconds;
			_timer.Start();
		}

		private Statistics Finish()
		{
			_timer.Stop();
			if (FoundWords.Count > 0)
				_apiController.PostWords(
					FoundWords
						.Select(x => x.Name)
						.OrderBy(x => x.Length)
				).Wait();

			var result = _apiController.GetStatistics().Result;
			_apiController.FinishGame();
			return result;
		}

		protected void Move(Direction direction, int count = 1)
		{
			if (count == 0)
				return;
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			/* Here we have to wait after each requset otherwise moving will be incorrect.
			 * Commented code below does not work for unknown reason */
			//var movings = new Task<char[][]>[count - 1];
			//for (var i = 1; i < count; ++i)
			//	movings[i - 1] = _apiController.Move(direction, false);
			//Task.WaitAll(movings);
			for (var i = 1; i < count; ++i)
				_apiController.Move(direction, false).Wait();
			var map = _apiController.Move(direction).Result;

			lock (CurrentWindow)
			{
				var offset = direction.ToOffset();
				var newLocationX = CurrentWindow.X + offset.X * count;
				var newLocationY = CurrentWindow.Y + offset.Y * count;

				CurrentWindow.Refresh(map, new Point(newLocationX, newLocationY));
			}
		}

		protected void MoveTo(Point point)
		{
			var offset = point - new Size(CurrentWindow.Location);
			if (offset.X != 0)
			{
				var distanceToMove = Math.Abs(offset.X);
				var direction = offset.X < 0 ? Direction.Left : Direction.Right;
				if (CurrentWindow.MapWidth != null)
				{
					distanceToMove = Math.Min(
						Math.Abs(offset.X - CurrentWindow.MapWidth.Value),
						Math.Abs(offset.X + CurrentWindow.MapWidth.Value));
					if (distanceToMove < Math.Abs(offset.X))
						direction = direction.Reverse();
					else
						distanceToMove = Math.Abs(offset.X);
				}
				Move(direction, distanceToMove);
			}

			if (offset.Y != 0)
			{
				var optimalPath = Math.Abs(offset.Y);
				var direction = offset.Y < 0 ? Direction.Up : Direction.Down;
				if (CurrentWindow.MapHeight != null)
				{
					optimalPath = Math.Min(
						Math.Abs(offset.Y - CurrentWindow.MapHeight.Value),
						Math.Abs(offset.Y + CurrentWindow.MapHeight.Value));
					if (optimalPath < Math.Abs(offset.Y))
						direction = direction.Reverse();
					else
						optimalPath = Math.Abs(offset.Y);
				}
				Move(direction, optimalPath);
			}
		}

		protected void MoveCenterTo(Point center)
		{
			MoveTo(new Point(
				center.X - CurrentWindow.Bounds.Width / 2,
				center.Y - CurrentWindow.Bounds.Height / 2
			));
		}

		protected MapArea GetArea(Rectangle bounds)
		{
			if (bounds == CurrentWindow.Bounds)
				return CurrentWindow;

			var area = new MapArea(CurrentWindow.Map).Refresh(bounds);
			bounds = area.Bounds;
			var windowWidth = CurrentWindow.Bounds.Width;
			var windowHeight = CurrentWindow.Bounds.Height;

			var start = new Point
			{
				X = Math.Abs(bounds.Right - CurrentWindow.Bounds.Right)
				    < Math.Abs(bounds.Left - CurrentWindow.Bounds.Left)
					? bounds.Right - windowWidth : bounds.Left,

				Y = Math.Abs(bounds.Bottom - CurrentWindow.Bounds.Bottom)
				    < Math.Abs(bounds.Top - CurrentWindow.Bounds.Top)
					? bounds.Bottom - windowHeight : bounds.Top
			};
			var horizontalDirection = start.X == bounds.Left
				? Direction.Right : Direction.Left;
			var verticalDirection = start.Y == bounds.Top
				? Direction.Down : Direction.Up;
			MoveTo(start);

			do
			{
				do
				{
					if (IsGameOver)
						break;
					
					foreach (var point in CurrentWindow.Travel())
						area[point] = CurrentWindow[point];

					var offsetX = horizontalDirection == Direction.Left
						? CurrentWindow.Bounds.Left - bounds.Left
						: bounds.Right - CurrentWindow.Bounds.Right;
					if (offsetX <= 0)
						break;
					Move(horizontalDirection, Math.Min(windowWidth, offsetX));
				} while (true);

				if (IsGameOver)
					break;

				horizontalDirection = horizontalDirection.Reverse();
				var offsetY = verticalDirection == Direction.Up
					? CurrentWindow.Bounds.Top - bounds.Top
					: bounds.Bottom - CurrentWindow.Bounds.Bottom;
				if (offsetY <= 0)
					break;
				Move(verticalDirection, Math.Min(windowHeight, offsetY));
			} while (true);

#if DEBUG
			foreach (var line in area.Area.Select(x => new string(x)))
				Console.WriteLine(line);
			Console.WriteLine();
#endif

			return area;
		}
	}
}
