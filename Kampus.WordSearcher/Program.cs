﻿using Kampus.WordSearcher.Helpers;
using Kampus.WordSearcher.Logic;
using System;

namespace Kampus.WordSearcher
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			if (args.Length != 2)
				throw new ArgumentException("Expected command line:\n<server URI> <authentication key>");
			
			var gameClient = InitializeHelper.InitializeGameClient(args[0], args[1]);
			Console.WriteLine(gameClient.Run());
			if (gameClient is MapTraveller mapTraveller)
			{
				MapOutputHelper.OutputAsTxt(mapTraveller.Map);
				MapOutputHelper.OutputAsBmp(mapTraveller.Map);
			}

			Console.ReadLine();
		}
	}
}
