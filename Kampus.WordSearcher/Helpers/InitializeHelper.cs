﻿using Kampus.WordSearcher.Api;
using Kampus.WordSearcher.LetterParser;
using Kampus.WordSearcher.Logic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;

namespace Kampus.WordSearcher.Helpers
{
	internal static class InitializeHelper
	{
		internal static GameClient InitializeGameClient(string serverUri, string authKey)
		{
			var httpClient = InitializeHttpClient(int.MaxValue, 5000, 1000, serverUri, authKey);
			var letterParser = InitializeParser("Helpers/parser.txt");

			return new Logic.WordSearcher(
				letterParser,
				new ApiController(httpClient));
		}

		private static ILetterParser InitializeParser(string fileName)
		{
			var lines = File.ReadAllLines(fileName);
			var lettersCount = int.Parse(lines[0]);
			var letterRecognizer = new Dictionary<IReadOnlyList<IReadOnlyList<char>>, char>();

			var lineIndex = 1;
			for (var letterIndex = 0; letterIndex < lettersCount; ++letterIndex)
			{
				var value = lines[lineIndex++].Single();

				var matrix = new List<IReadOnlyList<char>>();
				while (lines[lineIndex] != "")
					matrix.Add(lines[lineIndex++].ToCharArray());
				++lineIndex;

				letterRecognizer[matrix] = value;
			}

			var pointsCount = int.Parse(lines[lineIndex++]);
			List<Point> keyPoints = null;
			if (pointsCount > 0)
			{
				keyPoints = new List<Point>();
				for (var pointIndex = 0; pointIndex < pointsCount; ++pointIndex)
				{
					var pair = lines[lineIndex++].Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
					keyPoints.Add(new Point(int.Parse(pair[0]), int.Parse(pair[1])));
				}
			}

			return new LetterParser.LetterParser(letterRecognizer, keyPoints);
		}

		private static HttpClientWithRetries InitializeHttpClient(
			int retriesCount,
			int tryTimeoutInMilliseconds,
			int retryTimeoutInMilliseconds,
			string serverUri,
			string authKey)
		{
			var httpClient = new HttpClientWithRetries(
				retriesCount,
				TimeSpan.FromMilliseconds(tryTimeoutInMilliseconds),
				TimeSpan.FromMilliseconds(retryTimeoutInMilliseconds))
			{
				BaseAddress = new Uri(serverUri)
			};
			httpClient.DefaultHeaders.Accept.Clear();
			httpClient.DefaultHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue("application/json"));
			httpClient.DefaultHeaders.Add("Authorization", $"token {authKey}");
			return httpClient;
		}
	}
}
