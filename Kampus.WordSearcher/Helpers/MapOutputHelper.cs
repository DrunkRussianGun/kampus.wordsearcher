﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Kampus.WordSearcher.Helpers
{
	internal static class MapOutputHelper
	{
		internal static void OutputAsTxt(char[][] map)
		{
			if (map == null)
				throw new ArgumentNullException(nameof(map));

			File.WriteAllLines("map.txt", map
				.Select(x => new string(x))
				.ToArray());
		}

		internal static void OutputAsBmp(char[][] map)
		{
			if (map == null)
				throw new ArgumentNullException(nameof(map));

			var width = map[0].Length;
			var height = map.Length;
			var bmp = new Bitmap(width, height);
			for (var y = 0; y < height; ++y)
				for (var x = 0; x < width; ++x)
					bmp.SetPixel(x, y, map[y][x] == '1' ? Color.Black :
						map[y][x] == '0' ? Color.White : Color.Gray);
			bmp.Save("map.bmp");
		}
	}
}
