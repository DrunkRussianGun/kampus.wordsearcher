﻿using Kampus.WordSearcher.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kampus.WordSearcher.Api.Helpers
{
	internal static class ReadHelper
	{
		internal static async Task<Session> ReadSession(HttpContent content)
		{
			var headers = content.Headers;
			int expires;
			DateTime lastModified;

			try
			{
				expires = int.Parse(headers.GetValues("Expires").Single());
				lastModified = DateTime.Parse(headers.GetValues("Last-Modified").Single());
			}
			catch
			{
				return null;
			}
			var map = ReadMap(await content.ReadAsStringAsync());

			return new Session
			{
				Duration = new TimeSpan(0, 0, expires),
				LastModified = lastModified,
				VisibleArea = map
			};
		}

		internal static async Task<char[][]> ReadMap(HttpContent content)
		{
			return ReadMap(await content.ReadAsStringAsync());
		}

		private static char[][] ReadMap(string content)
		{
			return GetSubstringsOf(content, '0', '1')
				.Select(x => x.ToCharArray())
				.ToArray();
		}

		private static IEnumerable<string> GetSubstringsOf(string str, params char[] symbols)
		{
			var start = 0;
			var length = str.Length;
			var symbolsSet = new HashSet<char>(symbols);
			for (var end = 0; end < length; ++end)
				if (!symbolsSet.Contains(str[end]))
				{
					if (start < end)
						yield return str.Substring(start, end - start);
					start = end + 1;
				}
		}
	}
}
