﻿using System.Net;
using System.Net.Http;

namespace Kampus.WordSearcher.Api.Helpers
{
	internal static class ThrowHelper
	{
		internal static void ThrowUnsuccessResponseException(HttpResponseMessage response)
		{
			throw new WebException(response.ToString());
		}

		internal static void ThrowUnexpectedResponseException(HttpResponseMessage response)
		{
			throw new WebException("Invalid format of the response:\n" + response.ToString());
		}
	}
}
