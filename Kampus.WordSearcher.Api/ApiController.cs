﻿using Kampus.WordSearcher.Api.Helpers;
using Kampus.WordSearcher.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kampus.WordSearcher.Api
{
	public class ApiController : IApiController
	{
		private readonly HttpClientWithRetries _client;

		public ApiController(HttpClientWithRetries client)
		{
			_client = client ?? throw new ArgumentNullException(nameof(client));
		}

		public async Task<Session> StartGame(bool test = false)
		{
			var query = "task/game/start" + (test ? "?test=true" : "");
			var response = await _client.PostAsync(query, null);
			ThrowIfResponseIsNotSuccess(response);

			var session = await ReadHelper.ReadSession(response.Content);
			if (session == null)
				ThrowHelper.ThrowUnexpectedResponseException(response);
			return session;
		}

		public async Task<Statistics> FinishGame()
		{
			var response = await _client.PostAsync("task/game/finish", null);
			ThrowIfResponseIsNotSuccess(response);
			return await response.Content.ReadAsAsync<Statistics>();
		}

		public async Task<Statistics> GetStatistics()
		{
			var response = await _client.GetAsync("task/game/stats");
			ThrowIfResponseIsNotSuccess(response);
			return await response.Content.ReadAsAsync<Statistics>();
		}

		public async Task<char[][]> Move(Direction direction, bool readAnswer = true)
		{
			var response = await _client.PostAsync($"task/move/{direction.ToString().ToLower()}", null);
			ThrowIfResponseIsNotSuccess(response);
			return readAnswer ? await ReadHelper.ReadMap(response.Content) : null;
		}

		public async Task<Statistics> PostWords(IEnumerable<string> words)
		{
			var response = await _client.PostAsJsonAsync("task/words/", words);
			ThrowIfResponseIsNotSuccess(response);
			return await response.Content.ReadAsAsync<Statistics>();
		}

		private void ThrowIfResponseIsNotSuccess(HttpResponseMessage response)
		{
			if (!response.IsSuccessStatusCode)
				ThrowHelper.ThrowUnsuccessResponseException(response);
		}
	}
}
