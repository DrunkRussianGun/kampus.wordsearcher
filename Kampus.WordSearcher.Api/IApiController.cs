﻿using Kampus.WordSearcher.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kampus.WordSearcher.Api
{
	public interface IApiController
	{
		Task<Session> StartGame(bool test = false);
		Task<Statistics> FinishGame();
		Task<Statistics> GetStatistics();
		Task<char[][]> Move(Direction direction, bool readAnswer = true);
		Task<Statistics> PostWords(IEnumerable<string> words);
	}
}
