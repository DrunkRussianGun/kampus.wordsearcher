﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Kampus.WordSearcher.Api
{
	public class HttpClientWithRetries : IDisposable
	{
		public HttpClientWithRetries(int retries, TimeSpan tryTimeout, TimeSpan retryTimeout)
		{
			_retries = retries;
			_tryTimeout = tryTimeout;
			_retryTimeout = retryTimeout;
			_client = new HttpClient();
		}
		
		public Uri BaseAddress
		{
			get => _client.BaseAddress;
			set => _client.BaseAddress = value;
		}
		public HttpRequestHeaders DefaultHeaders => _client.DefaultRequestHeaders;

		public async Task<HttpResponseMessage> GetAsync(string requestUri)
		{
			return await SendRequestWithRetries(ct => _client.GetAsync(requestUri, ct));
		}

		public async Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent content)
		{
			return await SendRequestWithRetries(ct => _client.PostAsync(requestUri, content, ct));
		}

		public async Task<HttpResponseMessage> PostAsJsonAsync<T>(string requestUri, T value)
		{
			return await SendRequestWithRetries(ct => _client.PostAsJsonAsync(requestUri, value, ct));
		}

		private async Task<HttpResponseMessage> SendRequestWithRetries(Func<CancellationToken, Task<HttpResponseMessage>> action)
		{
			for (var i = 0; i < _retries; i++)
			{
				try
				{
					var cts = new CancellationTokenSource(_tryTimeout);
					return await action(cts.Token);
				}
				catch (TaskCanceledException) { }
				Thread.Sleep(_retryTimeout);
			}
			throw new AggregateException("Server is not available");
		}

		public void Dispose() => _client.Dispose();

		private readonly HttpClient _client;
		private readonly int _retries;
		private readonly TimeSpan _tryTimeout;
		private readonly TimeSpan _retryTimeout;
	}
}