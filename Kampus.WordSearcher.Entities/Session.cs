﻿using System;

namespace Kampus.WordSearcher.Entities
{
	public class Session
	{
		public TimeSpan Duration { get; set; }
		public DateTime LastModified { get; set; }
		public char[][] VisibleArea { get; set; }
	}
}
