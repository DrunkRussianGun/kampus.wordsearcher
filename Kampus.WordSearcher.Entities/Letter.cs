﻿using System.Drawing;

namespace Kampus.WordSearcher.Entities
{
	public class Letter
	{
		public char Name { get; set; }
		public Rectangle Location { get; set; }

		public Letter(char name, Rectangle location)
		{
			Name = name;
			Location = location;
		}
	}
}
