﻿using System;
using System.Drawing;

namespace Kampus.WordSearcher.Entities
{
	public class MapGeometry
	{
		public static readonly MapGeometry UnknownMap = new MapGeometry();

		public int? MapWidth { get; }
		public int? MapHeight { get; }

		public MapGeometry(int? mapWidth = null, int? mapHeight = null)
		{
			MapWidth = mapWidth;
			MapHeight = mapHeight;
		}

		public Point Normalize(Point point)
		{
			if (MapWidth != null)
			{
				point.X %= MapWidth.Value;
				if (point.X < 0)
					point.X += MapWidth.Value;
			}
			if (MapHeight != null)
			{
				point.Y %= MapHeight.Value;
				if (point.Y < 0)
					point.Y += MapHeight.Value;
			}
			return point;
		}

		public Rectangle Normalize(Rectangle rectangle)
		{
			rectangle.Location = Normalize(rectangle.Location);
			return rectangle;
		}

		public Rectangle? GetIntersection(Rectangle a, Rectangle b)
		{
			var normalizedA = Normalize(a);
			var normalizedB = Normalize(b);

			(int start, int end) GetIntersectionOfSegments(int start1, int end1, int start2, int end2, int? fieldSize)
			{
				var start = Math.Max(start1, start2);
				var end = Math.Min(end1, end2);
				if (start < end || fieldSize == null)
					return (start, end);
				
				start = Math.Max(start1, start2 + fieldSize.Value);
				end = Math.Min(end1, end2 + fieldSize.Value);
				if (start < end)
					return (start, end);

				start = Math.Max(start1 + fieldSize.Value, start2);
				end = Math.Min(end1 + fieldSize.Value, end2);
				return (start, end);
			}

			var (left, right) = GetIntersectionOfSegments(
				normalizedA.Left, normalizedA.Right,
				normalizedB.Left, normalizedB.Right,
				MapWidth);
			var (top, bottom) = GetIntersectionOfSegments(
				normalizedA.Top, normalizedA.Bottom,
				normalizedB.Top, normalizedB.Bottom,
				MapHeight);
			if (left >= right || top >= bottom)
				return null;

			return new Rectangle(left, top, right - left, bottom - top);
		}

		public bool IsPointInside(Rectangle rectangle, Point point)
		{
			return AreIntersecting(rectangle, new Rectangle(point, new Size(1, 1)));
		}

		public bool AreIntersecting(Rectangle a, Rectangle b)
		{
			return GetIntersection(a, b) != null;
		}
	}
}
