﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Kampus.WordSearcher.Entities
{
	public class MapArea
	{
		public char[][] Area { get; private set; }
		public Rectangle Bounds { get; private set; }

		private MapGeometry _map;
		public MapGeometry Map
		{
			get => _map;
			set
			{
				_map = value ?? MapGeometry.UnknownMap;
				Bounds = _map.Normalize(Bounds);
			}
		}

		public int X => Bounds.X;
		public int Y => Bounds.Y;
		public int Width => Bounds.Width;
		public int Height => Bounds.Height;
		public int? MapWidth => _map.MapWidth;
		public int? MapHeight => _map.MapHeight;
		public Point Location => Bounds.Location;

		public MapArea(MapGeometry map = null)
		{
			Map = map;
		}

		public MapArea Refresh(Rectangle bounds)
		{
			var width = bounds.Width;
			var height = bounds.Height;

			Area = new char[height][];
			for (var i = 0; i < height; ++i)
				Area[i] = new char[width];

			Bounds = _map.Normalize(bounds);
			return this;
		}

		public MapArea Refresh(char[][] area, Point location = default(Point))
		{
			Area = area ?? throw new ArgumentNullException(nameof(area));

			var size = GetSize(area);
			Bounds = new Rectangle(_map.Normalize(location), size);
			return this;
		}

		public char this[int x, int y]
		{
			get
			{
				var innerPoint = ToInnerPoint(new Point(x, y));
				return Area[innerPoint.Y][innerPoint.X];
			}
			set
			{
				var innerPoint = ToInnerPoint(new Point(x, y));
				Area[innerPoint.Y][innerPoint.X] = value;
			}
		}

		public char this[Point point]
		{
			get
			{
				var innerPoint = ToInnerPoint(point);
				return Area[innerPoint.Y][innerPoint.X];
			}
			set
			{
				var innerPoint = ToInnerPoint(point);
				Area[innerPoint.Y][innerPoint.X] = value;
			}
		}

		private Size GetSize(char[][] area)
		{
			var widths = area
				.Select(x => x.Length)
				.Distinct()
				.ToArray();
			if (widths.Length != 1)
				throw new ArgumentException("Area is not a rectangle", nameof(area));

			return new Size(widths[0], area.Length);
		}

		private Point ToInnerPoint(Point point)
		{
			var normalizedPoint = _map.Normalize(point);
			if (_map.MapWidth != null)
			{
				var newX = normalizedPoint.X + _map.MapWidth.Value;
				if (Bounds.Left <= newX && newX < Bounds.Right)
					normalizedPoint.X = newX;
			}
			if (_map.MapHeight != null)
			{
				var newY = normalizedPoint.Y + _map.MapHeight.Value;
				if (Bounds.Top <= newY && newY < Bounds.Bottom)
					normalizedPoint.Y = newY;
			}

			if (!Bounds.Contains(normalizedPoint))
				throw new IndexOutOfRangeException("Cell is outside of map area");
			return normalizedPoint - new Size(Bounds.Location);
		}

		public IEnumerable<Point> Travel(Rectangle bounds = default(Rectangle))
		{
			if (bounds == default(Rectangle))
				bounds = Bounds;
			for (var y = bounds.Top; y < bounds.Bottom; ++y)
				for (var x = bounds.Left; x < bounds.Right; ++x)
					yield return new Point(x, y);
		}

		public Rectangle GetBoundarySide(Direction side)
		{
			switch (side)
			{
				case Direction.Up:
					return new Rectangle(Bounds.Location, new Size(Bounds.Width, 1));
				case Direction.Left:
					return new Rectangle(Bounds.Location, new Size(1, Bounds.Height));
				case Direction.Down:
					return new Rectangle(Bounds.Left, Bounds.Bottom - 1, Bounds.Width, 1);
				case Direction.Right:
					return new Rectangle(Bounds.Right - 1, Bounds.Top, 1, Bounds.Height);
				default:
					throw new ArgumentException("Such direction is not supported", nameof(side));
			}
		}
	}
}
