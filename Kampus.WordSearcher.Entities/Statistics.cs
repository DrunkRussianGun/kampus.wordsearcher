﻿namespace Kampus.WordSearcher.Entities
{
	public class Statistics
	{
		public int Points { get; set; }
		public int? Words { get; set; }
		public int? Moves { get; set; }

		public override string ToString()
		{
			var result = $"Points = {Points}";
			if (Words != null)
				result += $", Words = {Words}";
			if (Moves != null)
				result += $", Moves = {Moves}";
			return result;
		}
	}
}
