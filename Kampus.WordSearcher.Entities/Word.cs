﻿using System;
using System.Drawing;

namespace Kampus.WordSearcher.Entities
{
	public class Word
	{
		public string Name { get; }
		public Rectangle Location { get; }

		public Word(string name, Rectangle location)
		{
			Name = name ?? throw new ArgumentNullException(nameof(name));
			Location = location;
		}

		public override bool Equals(object obj) => Equals(obj as Word);
		public bool Equals(Word other)
		{
			if (ReferenceEquals(this, other))
				return true;
			return !ReferenceEquals(other, null)
				&& Name == other.Name;
		}

		public override int GetHashCode() => Name.GetHashCode();
		public override string ToString() => Name;

		public static bool Equals(Word left, Word right)
		{
			if (!ReferenceEquals(left, null))
				return left.Equals(right);

			return ReferenceEquals(right, null);
		}

		public static bool operator ==(Word left, Word right) => Equals(left, right);
		public static bool operator !=(Word left, Word right) => !Equals(left, right);
	}
}
