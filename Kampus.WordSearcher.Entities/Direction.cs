﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Kampus.WordSearcher.Entities
{
	public enum Direction { Up, Left, Right, Down };

	public static class DirectionExtensions
	{
		private static readonly IReadOnlyDictionary<Direction, Point> OffsetsByDirection
			= new Dictionary<Direction, Point>
			{
				{ Direction.Up, new Point(0, -1) },
				{ Direction.Left, new Point(-1, 0) },
				{ Direction.Down, new Point(0, 1) },
				{ Direction.Right, new Point(1, 0) }
			};
		private static readonly IReadOnlyDictionary<Direction, Direction> ReverseDirections
			= new Dictionary<Direction, Direction>
			{
				{ Direction.Up, Direction.Down },
				{ Direction.Left, Direction.Right },
				{ Direction.Down, Direction.Up },
				{ Direction.Right, Direction.Left }
			};

		public static bool IsHorizontal(this Direction direction)
		{
			return direction == Direction.Left || direction == Direction.Right;
		}

		public static bool IsVertical(this Direction direction)
		{
			return direction == Direction.Up || direction == Direction.Down;
		}

		public static Direction Reverse(this Direction direction)
		{
			return ReverseDirections[direction];
		}

		public static Point ToOffset(this Direction direction)
		{
			return OffsetsByDirection[direction];
		}
	}
}
