﻿using Kampus.WordSearcher.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Kampus.WordSearcher.LetterParser
{
	public class LetterParser : ILetterParser
	{
		private readonly IReadOnlyDictionary<IReadOnlyList<IReadOnlyList<char>>, char> _recognizer;
		private readonly IReadOnlyCollection<Point> _keyPoints;

		public LetterParser(
			IReadOnlyDictionary<IReadOnlyList<IReadOnlyList<char>>, char> recognizer,
			IReadOnlyCollection<Point> keyPoints = null)
		{
			_recognizer = recognizer ?? throw new ArgumentNullException(nameof(recognizer));
			_keyPoints = keyPoints;
		}

		public Letter Parse(char[][] letter)
		{
			var matchingLetter = _recognizer.Keys
				.FirstOrDefault(mask => Matches(letter, mask));

			if (matchingLetter == null)
				return null;
			return new Letter(
				_recognizer[matchingLetter],
				new Rectangle(0, 0, matchingLetter[0].Count, matchingLetter.Count)
			);
		}

		private bool Matches(char[][] letter, IReadOnlyList<IReadOnlyList<char>> mask)
		{
			if (_keyPoints != null)
			{
				foreach (var point in _keyPoints)
					if (letter[point.Y][point.X] != mask[point.Y][point.X])
						return false;
			}
			else
			{
				var width = mask[0].Count;
				var height = mask.Count;
				for (var y = 0; y < height; ++y)
					for (var x = 0; x < width; ++x)
						if (letter[y][x] != mask[y][x])
							return false;
			}
			return true;
		}
	}
}
