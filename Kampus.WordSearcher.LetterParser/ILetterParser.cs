﻿using Kampus.WordSearcher.Entities;

namespace Kampus.WordSearcher.LetterParser
{
	public interface ILetterParser
	{
		Letter Parse(char[][] letter);
	}
}
